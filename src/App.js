import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <p>MercDev CI/CD Home Work</p>
      </header>
    </div>
  );
}

export default App;
